﻿<%@ Page Title="" Language="C#" MasterPageFile="Home.master" AutoEventWireup="true" CodeFile="SuiteRoom.aspx.cs" Inherits="SuiteRoom" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <div>
			 <h3>Suite</h3>
             <hr />
			 <img src="img/suiteslide.jpg"/>
			 <p>Suite is a spacious and contemporary accommodation to indulge the modern business traveler 
                 with plush furnishings and a common verandah with a lovely view of the CBD.</p>
			 <h4>Features and Facilities:</h4>
               <div class="row-fluid">
                <div class="span6">
                    <ul>
				      	<li>Individually controlled air-conditioning</li>
				      	<li>Free in-house movie channels</li>
				      	<li>Mini bar/Refrigerator</li>
				      	<li>Electronic safe deposit box^</li>
				      	<li>Luxurious toiletries</li>
                    </ul>
                 </div>
                <div class="span6">
                    <ul>
				      	<li>Colour Television with remote control</li>
				      	<li>Selected Foxtel channels</li>
                        <li>Hair Dryer</li>
                        <li>Use of offsite fitness centre</li>
                        <li>Free WiFi</li>
				    </ul>
                </div>
                </div>
                <h4>Residence Services:</h4>
             <div class="row-fluid">
                <div class="span6">
                    <ul>
				      	<li>Massage</li>
                    </ul>
                 </div>
                <div class="span6">
                    <ul>
				      	<li>Dry cleaning and Laundry services</li>
				    </ul>
                </div>
              </div>
                <h4>Safety and Security:</h4>
              <div class="row-fluid">
                <div class="span6">
                    <ul>
				      	<li>Smoke detector and sprinkler</li>
                    </ul>
                 </div>
                <div class="span6">
                    <ul>
				      	<li>All doors are installed with safety latch and eye viewer</li>
				    </ul>
                </div>
			 </div>
            </div>
</asp:Content>

