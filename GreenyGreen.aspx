﻿<%@ Page Title="" Language="C#" MasterPageFile="Home.master" AutoEventWireup="true" CodeFile="GreenyGreen.aspx.cs" Inherits="GreenyGreen" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">


     <div class="row-fluid">
        <h2 align ="center"> Greeny Green Operators</h2>
         <hr/>
              <ul class="thumbnails">
            <div class="span12">
                <p align ="center"><b>OFFICIAL ECO-PARTNERS OF ALLEGRA HOTELS</b> </p>
            <p align ="center"><b> ECO Certified tourism businesses.</b></p>
<h4 align ="left">Our Belief</h4>
<p>We have a dream and the dream is to ensure that whoever selects us as a tour operator gets the best return of investment for his money, and we believe that
     the smile on the customer's face at the end of a tour is worth much more than the accolades and the awards that we have recieved.</p>

                <h3 align ="center"><i>Our Tour Offerings are as Follows</i></h3>
                <div class="row span12">
                 <div class ="span5">
                     <ul class ="thumbnails">
                <article class="thumbnail"/>
                    <img src="img/MB.jpg">
                     </div>
                <div class ="span6">
                     <b>Mt Buller Day Tours</b>
                 <p> 
                   Mt. Buller - an exhilarating winter experience. 
                     This alpine wonderland is nestled high on the ski fields with quaint lodges, 
                     luxury hotels, cosy restaurants and cafes. Snowshoe walking, boardriding, 
                     skiing, tobogganing or simply enjoy the village atmosphere.Picturesque Yarra Valley Country 
                     townships of Yea and Bonnie Doon Mansfield 
                     </p> 
                    </div>
                </div>
                
               <div class="row span12">
                 <div class ="span5">
                     <ul class ="thumbnails">
                <article class="thumbnail"/>
                    <img src="img/MDT.jpg">
                     </div>
                <div class ="span6">
                     <b>Melbourne City Day</b>
                 <p> 
                   Welcome to Melbourne - Heart of Australia's classic south. Home to the Australian Open Tennis, 
                     Australian rules football, the Melbourne Cup and for a short time Australia's capital. Melbourne
                      is known for its gardens, restaurants, cafes and markets, arcades and laneways, galleries, the arts and its relaxed elegant charm. 
                     </p> 
                   </div>
              </div>
                
               <div class="row span12">
                 <div class ="span5">
                     <ul class ="thumbnails">
                <article class="thumbnail"/>
                    <img src="img/MRP.jpg">
                     </div>
                <div class ="span6">
                     <b>Mornington Peninsula with Sorrento</b>
                 <p> 
                  Spectacular views of rolling hills, pastures and vineyards. 
                     Beach side hamlets of Port Phillip Bay Sorrento - an upper-class
                      seaside resort Enjoy a morning tea break - perhaps 
                     a tea or coffee right on the waterfront at The Baths 
                     in Sorrento Ample time to discover this popular resort town at 
                     your own pace
                      Look out for summer activities right on the front beach  .
                     
                     </p> 
                    </div>
                </div>

                <div class="row span12">
                 <div class ="span5">
                     <ul class ="thumbnails">
                <article class="thumbnail"/>
                    <img src="img/SHD.jpg">
                     </div>
                <div class ="span6">
                     <b>Sovereign Hill Tours</b>
                 <p> 
                  Gold - 1850 - The world focussed on Ballarat as the cry 'Gold' echoed around the globe. 
                     People from all nations poured into Ballarat to seek a fortune, 
                     many found the real Australia and decided to make this great land their home. 
                     Today, you too can capture the excitement of these bawdy times as you visit the scene of revolt, rebellion and the emergence of democracy.
                     </p> 
              </div>
             </div>

                <div class="row span12">
                 <div class ="span5">
                     <ul class ="thumbnails">
                <article class="thumbnail"/>
                    <img src="img/River Mite.jpg">
                     </div>
                    <div class ="span6">
                     <b>Puffing Billy with Wildlife and Winery Lunch </b>
                 <p> 
                   Ride on Australia's most notable narrow gauge railway through the spectacular Blue Dandenong Ranges with bellbirds and kookaburras echoing the steam train's whistle. Tour through the richness of the Yarra Valley - a pastoral experience of farms and vineyards, haystacks and wine grapes. Healesville Sanctuary - a truly Australian experience - cradled in the forested foothills of the Great Divide where you can surround yourself with Australian wildlife.
                     </p> 
              </div>
             </div>
                </div>

</asp:Content>
