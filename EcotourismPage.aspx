﻿<%@ Page Title="" Language="C#" MasterPageFile="Home.master" AutoEventWireup="true" CodeFile="EcotourismPage.aspx.cs" Inherits="EcotourismPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">


    <div class="row-fluid">
        <h2 align ="center">Our Vision</h2>
        <hr/>
        <ul class="thumbnails">
     <div class="span12">
              <article class="thumbnail"/>
        <p align ="center"><img src="img\tourism-marketing.jpg"></p>
        <p><b>Our Vision in ecotourism :</b> Whether you  travel to relax, explore or to learn more about the envoirnment, we know choosing the right eco friendly operator can be a great hassle.
            So, we do it for you. Our team of dedicated professionals carefully select the eco friendly operators
             after weighing the pros and cons so that our customers can have an unforgettable experience at an unbeatable price .
             We and our partners do everything we can to protect the environment around us for future generations.
            The impact of our choice can be felt on the ground and make all the difference between what's lost and what's saved. 
            <b> We and our partners are ECO Certified tourism businesses.</b>
        </p>
     </div>
            <br ><br />
            <br ><br />
          
          &nbsp;  &nbsp;  &nbsp;  &nbsp;  &nbsp;  &nbsp;
            <h2 ALIGN ="CENTER">Our Eco Friendly Partners</h2>
        
         <div class="row span12">
          <ul class="thumbnails">
            <div class="span4">
              <article class="thumbnail"/>
              <img src="img\nightofmelbourne.jpg"/>
                <article class="thumbnail-caption text-center">
                     </article>
                    &nbsp;    
                </div>              
                <div class="span8">
                  <B align ="center" >Green Melbourne Operators</B>
             
              <p>These  operators are one of the finest in Melbourne.
             Thier tours and  programs emphasize understanding the 
                  community approaches toward  the envoirnment, as well as the underlying conservation, capacity, social, political, and economic issues.
                 They ensure that you enjoy the pleasures and save the envoirnment while doing so. 
                  Thier main aim is to <i>inspire environmentally sustainable and culturally responsible tourism.</i>
               <a href="EcoTravelOperators.aspx"> <p>Click here to know more:</p> </a>
                   </p>
               </div> 
           </div>
              
        <div class="row span12">
              <ul class="thumbnails">
            <div class="span4">
              <article class="thumbnail"/>
               <a href="EcoTravelOperators.aspx"> <img src="img\Go Green.jpg" alt="Koala"/></a>
                <article class="thumbnail-caption text-center">
                     </article>
                    &nbsp;    
                </div>                 
                <div class="span8">
                <B align ="center" >Local Melbourne Operators</B>
             
              <p>We are Melbourne’s premier small group day tour operator. Since December 2010 we have operated small group day tours to
                   Melbourne’s iconic day tour destinations, showing our customers some great sights and - more importantly – some great times.

                   Our Melbourne-based day tours operate daily with pickups from all major accommodation in Melbourne’s inner city area.
                  We provide the best services for people from every demographic.
                  
                <a href="LocalMelbourneOperators.aspx"> <p>Click here to know more:</p> </a>
                
                </div>
       </div>
                               <p></p>

              <p></p>
                   </p>
    <div class="row span12">
           <ul class="thumbnails">
            <div class="span4">
              <article class="thumbnail"/>
               <a href="EcoTravelOperators.aspx"> <img src="img\greenygreen.jpg" alt="Koala"/></a>
                <article class="thumbnail-caption text-center">
                     </article>
                    &nbsp;    
                </div>                 
                <div class="span8">
                  <B align ="center" >Greeny Green Operators</B>
             
              <p>These  operators are unique as they offer eco friendly tours of MT Bueler, wineries, Great Ocean Road ,Philip Island Penguin Parade,Melbourne City day,Soviergn Hill,and Puffing Billy Steam Train.

                  Mornington PeninsulaThey are gauranteed to provide a great amount of satisfaction and enjoyment for the entire family.They take the saying "there is no age bar for having fun and implement it very very successfully.
                  
               <a href="GreenyGreen.aspx"> <p>Click here to know more:</p> </a>
                   </p>
                </div> 
     </div>

            <div class="span12">
              <h2 align ="center">Our Reccomendations</h2>
              <p>All three eco friendly operators are amazing.However, after analysing the data collected as feedback from the customers over the years we gave realized the following things
                  <ol>
                      <li>Green MelBourne Operators</li>
                      <p>This is highly reccomended for the people who love adventure.Ideal for bagpackers, students and basically anyone who is young and is a certified daredevil.</p>
                      <li>LocalMelbourneOperators</li>                   
                  <p>This is highly reccomended for couples, and people on a holiday, looking for a good quiet and relaxing time.Nothing dangerous, but this tour promises loads of fun.
                      It has been rated as a favourite by many couples.
                  </p>
                      <li>Greeny Green Operators</li>
                      <p>They are really unique operators and offer the best of both worlds.
                          Ideal for people of all ages, family, friends, couples and basically everyone under the sun.
                          this tour provider has something to satisfy everyone's dream holiday.

                      </p>
                  </ol>
              </p>
            </div>
       </ul>
        
    <h2 align ="center">What we do for the Envoirnment</h2>
    <p>From the last four years, we have been winning the award for best service provider in the hospitality industry, and our contribution towards safegaurding the envoirnment have been globally recognized and appreciated.
       We were the pioneers in the industry who banned the use of plastic bags for all purposes and propagated the use of papaer bags.We have gotten rid of all electrical appliances that emitted CFC's .Our participation in the green moment bolsters the fact that we are serious about the envoirnment and it is time we start to safegaurd the envoirnment for the generations to come.

       
            </p>
</asp:Content>

