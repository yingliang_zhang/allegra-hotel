﻿<%@ Page Title="" Language="C#" MasterPageFile="Home.master" AutoEventWireup="true" CodeFile="EcoTravelOperators.aspx.cs" Inherits="EcoTravelOperators" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">

    <div class="row-fluid">
        <h2 align ="center">Extreme Green Operators Melbourne</h2>
        <hr/>
              <ul class="thumbnails">
            <div class="span12">
              <article class="thumbnail"/>
                <p align ="center"><img src="img\ecotourismpic.jpg"></p>
                <p><b>Proud to be Partners with Allegra Hotels</b> Whether you want to unwind  by taking a trip down memory lane, or tour the city with a loved one, you have come to the right place.
                    We have the best tours which give the traveller a great perspective .We have been winning the award for the best eco friendly operator in the country from the last three years.
                    <b> We and our partners are ECO Certified tourism businesses.</b>
                </p>
            </div>

            <h3 align =" center">Services offered by Green Operators</h3>
                  
              <div class="row span12">
                <div class ="span3">
                     <ul class ="thumbnails">
                    <article class="thumbnail"/>
                    <img src="img/rsz_extreme-surfing.jpg">
                    <article class="thumbnail-caption text-center">   
                </div>
                <div class ="span6">
                     <b>Surfing</b>
                 <p> 
                     A great way to feel the adrenalin pumping through, and have a great time.Surfing is provided by this operator and is a part of the eco-friendly tour.
                     </p> 
                  </div>
               </div>
                <br><br />
                  
          <div class="row span12">
             <div class ="span3">
                     <ul class ="thumbnails">
                <article class="thumbnail"/>
                    <img src="img/hiking.jpg">
                    <article class="thumbnail-caption text-center">   
              </div>
            <div class ="span6">
                     <b>Hiking</b>
                 <p> 
                    We have tours that entails hiking for  a day.We start early in the  morning and return in the evening.Hiking is done in a group.An expert will be there at all times.
                     Hiking equipment will be provided by us.
                     </p> 
            </div>
          </div>
                  
            <div class="row span12">
                <div class ="span3">
                     <ul class ="thumbnails">
                <article class="thumbnail"/>
                    <img src="img/cycling.jpg">
                    <article class="thumbnail-caption text-center">   
                </div>
                <div class ="span6">
                     <b>Cycling</b>
                 <p>
                     We offer cycyclotourism, involves touring and exploration
                      or sightseeing by bicycle for leisure.
                     A guide will accompany you at all times to ensure a great experience. 
                     Bicycling tour is a great recreation
                      for all those who are fit and like cycling.
                    
                     </p> 
                </div>
              </div>
                  
           <div class="row span12">
            <div class ="span3">
                     <ul class ="thumbnails">
                <article class="thumbnail"/>
                    <img src="img/skating.jpg">
                    <article class="thumbnail-caption text-center">   
              </div>
             <div class ="span6">
                     <b>Skating</b>
                 <p> 
                    Skating is a wonderful way for the whole family to enjoy and have fun at the same time. 
                        During our tour,watch the professionals.      
                        Interested candidates  can learn skills such as jumps, spins and footwork. 
                     </p> 
                </div>
             </div>
                  
               <div class="row span12">
                <div class ="span3">
                     <ul class ="thumbnails">
                <article class="thumbnail"/>
                    <img src="img/itinerari6.jpg">
                    <article class="thumbnail-caption text-center">   
                </div>
                 <div class ="span6">
                     <b>Rock Climbing</b>
                 <p> 
                     Discover the natural world
                      around and within you. 
                     Rock Climbing will bring you to lots 
                     of lovely places and will definitely give you a unique perspective
                      on the world below you.
                      You'll learn a couple of things about yourself and the people you climb with.                       

                     </p> 
                </div>
              </div>

     <div class ="span11">
         <h2 align ="center">About US</h2>
    <p>We are a  eco friendly operator who believe in serving customers with all our heart and try to ensure that everone who comes to visit this beautiful city  creates special memories .</p>
                 </div> 
                  </div>
</asp:Content>